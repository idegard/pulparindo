package com.idegard.pulparindo;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.security.Security;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.idegard.pulparindo.exceptions.CertificadoException;
import com.idegard.pulparindo.exceptions.FirmaException;
import com.idegard.pulparindo.exceptions.PdfInvalidoException;
import com.idegard.pulparindo.exceptions.PropiedadesException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfSignatureAppearance.RenderingMode;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;

/**
 * Metodos estaticos para el firmado de pdf 
 * @author aiolivaresl
 */
public class Firma {
	
	private static ExternalDigest digest = new BouncyCastleDigest();
	
	private static final String SIN_MOTIVO = "";
	private static final String SIN_UBICACION = "";
	private static final String DEFAULT_POS = "0";
	private static final String DEFAULT_TAM = "50";
	private static final String DEFAULT_PAG = "1";
	private static final String DEFAULT_FONDO = "escudo.jpg";
	
	
	/**
	 * <strong>Propiedades de la firma (opcionales):</strong>
	 * <p>
	 * <strong>Motivo</strong> - razon de la firma (en blanco por default)<br>
	 * <strong>Ubicacion</strong> - localidad de firmado (en blanco por default)<br>
	 * <strong>Nombre</strong> - nombre del campo visual de la firma (autogenerado por iText por default)<br>
	 * <strong>PosX</strong> - posicion en x de la firma visible (0 por default)<br>
	 * <strong>PosY</strong> - posicion en y de la firma visible (0 por default)<br>
	 * <strong>Tamx</strong> - ancho de la firma visible (50 por default)<br>
	 * <strong>TamY</strong> - alto de la firma visible (50 por default)<br>
	 * <strong>Pagina</strong> - numero de pagino de la firma visible (1 por default)<br>
	 * <strong>Autografa</strong> - imagen encodeada Base64 para usarse como firma autografa (ninguno por default)
	 * 				Nota: de darse una firma autografa unicamente esta imagen sale en el campo visible
	 * 				quitando logo de fondo y detalles de la firma<br>
	 * <strong>Fondo</strong> - imagen de fondo de la firma visible (logo UAEM por default)<br>
	 * </p>
	 * @author aiolivaresl
	 */
	public enum Propiedades{
		Motivo,Ubicacion,Nombre,PosX,PosY,TamX,TamY, Pagina, Autografa, Fondo}
		
	/**
	 * @param entrada Pdf a firmar en bytes
	 * @param certificado Archivo p12 con que se firmara (en bytes)
	 * @param contrasena Contrasena del archivo
	 * @param alias Identificador del certificado a usar, null para usar el primero
	 * @param propiedades Mapa de {@link Propiedades opciones} del firmado
	 * @return Pdf firmado (en bytes)
	 * @throws CertificadoException Error de certificado
	 * @throws FirmaException Error en el proceso de firma 
	 */
	public static byte[] firmar(byte[] entrada, byte[] certificado, String contrasena, String alias, Map<Propiedades,String> propiedades) throws CertificadoException, FirmaException{
		return creaFirma(entrada, certificado, contrasena, alias, propiedades);
	}
	
	/**
	 *Igual que {@link #firmar(byte[], byte[], String, String, Map) Firma.firmar(entrada,certificado,contrasena,null,null)}
	 */
	public static byte[] firmar(byte[] entrada, byte[] certificado, String contrasena) throws CertificadoException, FirmaException{
		return firmar(entrada, certificado, contrasena, null, null);
	}
	
	/**
	 *Igual que {@link #firmar(byte[], byte[], String, String, Map) Firma.firmar(entrada,certificado,contrasena,null,propiedades)}
	 */
	public static byte[] firmar(byte[] entrada, byte[] certificado, String contrasena, Map<Propiedades,String> propiedades) throws CertificadoException, FirmaException{
		return firmar(entrada, certificado, contrasena, null, propiedades);
	}
	
	/**
	 *Metodo interno de firmado
	 *@see {@link #firmar(byte[], byte[], String, String, Map) Firma.firmar(entrada,certificado,alias,propiedades)}
	 */
	private static byte[] creaFirma(byte[] entrada, byte[] certificado, String contrasena, String alias, Map<Propiedades,String> propiedades) throws CertificadoException, FirmaException {

		propiedades=propiedades==null?new HashMap<Propiedades,String>():propiedades;
		propiedades.put(Propiedades.Fondo, propiedades.get(Propiedades.Fondo)!=null?propiedades.get(Propiedades.Fondo):fondoEncode());
		propiedades.put(Propiedades.Motivo, propiedades.get(Propiedades.Motivo)!=null?propiedades.get(Propiedades.Motivo):SIN_MOTIVO);
		propiedades.put(Propiedades.Ubicacion, propiedades.get(Propiedades.Ubicacion)!=null?propiedades.get(Propiedades.Ubicacion):SIN_UBICACION);
		propiedades.put(Propiedades.PosX, propiedades.get(Propiedades.PosX)!=null?propiedades.get(Propiedades.PosX):DEFAULT_POS);
		propiedades.put(Propiedades.PosY, propiedades.get(Propiedades.PosY)!=null?propiedades.get(Propiedades.PosY):DEFAULT_POS);
		propiedades.put(Propiedades.TamX, propiedades.get(Propiedades.TamX)!=null?propiedades.get(Propiedades.TamX):DEFAULT_TAM);
		propiedades.put(Propiedades.TamY, propiedades.get(Propiedades.TamY)!=null?propiedades.get(Propiedades.TamY):DEFAULT_TAM);
		propiedades.put(Propiedades.Pagina, propiedades.get(Propiedades.Pagina)!=null?propiedades.get(Propiedades.Pagina):DEFAULT_PAG);

		try{
		Security.addProvider(new BouncyCastleProvider());
		Certificado cert = new Certificado(certificado,contrasena,alias);
				
        PdfReader reader = new PdfReader(entrada);
        ByteArrayOutputStream salida = new ByteArrayOutputStream();
        PdfStamper stamper = PdfStamper.createSignature(reader, salida, '\0',null,true);

        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        
        appearance.setReason(propiedades.get(Propiedades.Motivo));
        appearance.setLocation(propiedades.get(Propiedades.Ubicacion));

        if(propiedades.get(Propiedades.Fondo)!=null)
        	appearance.setImage(Image.getInstance(Base64.decodeBase64(propiedades.get(Propiedades.Fondo))));
              
        if(propiedades.get(Propiedades.Autografa)==null){
        	appearance.setRenderingMode(RenderingMode.DESCRIPTION);
        }else{
        	appearance.setRenderingMode(RenderingMode.GRAPHIC);
        	appearance.setSignatureGraphic(Image.getInstance(Base64.decodeBase64(propiedades.get(Propiedades.Autografa))));
        }        
        
        float posicionX=Float.parseFloat(propiedades.get(Propiedades.PosX));
        float posicionY=Float.parseFloat(propiedades.get(Propiedades.PosY));
        float tamanoX=Float.parseFloat(propiedades.get(Propiedades.TamX));
        float tamanoY=Float.parseFloat(propiedades.get(Propiedades.TamY));
        int pagina=Integer.parseInt(propiedades.get(Propiedades.Pagina));
                
        appearance.setVisibleSignature(new Rectangle(posicionX,posicionY,tamanoX,tamanoY), pagina, propiedades.get(Propiedades.Nombre));
        
        ExternalSignature externalSignature = new PrivateKeySignature(cert.getPrivateKey(), "SHA-256", "BC");
        MakeSignature.signDetached(appearance, digest, externalSignature, cert.getCertificateChain(), null, null, null, 0, CryptoStandard.CMS);
        
        return salida.toByteArray();
		} 
		catch (InvalidPdfException e) {
			throw new PdfInvalidoException("Verificar archivo de entrada");
		}
		catch (CertificadoException e) {
			throw e;
		}
		catch(NumberFormatException e){
			throw new PropiedadesException("Campo numerico incorrecto");
		}
		catch(IOException e){
			throw new PropiedadesException("Campo imagene incorrecto");
		}
		catch(IllegalArgumentException e){
			throw new PropiedadesException("Campos nombre y/o pagina incorrecto");
		}
		catch (Exception e) {
			throw new FirmaException("Error irrecuperable de sistema: "+e.getMessage());
		}
    }
	
	private static String fondoEncode(){
		byte[] bytes  = new byte[15360];
		DataInputStream dataIs = new DataInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream(DEFAULT_FONDO));
		try {
			dataIs.readFully(bytes);
		} catch (IOException e) {
		}
		return Base64.encodeBase64String(bytes);
	}
}
