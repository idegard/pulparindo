package com.idegard.pulparindo.exceptions;

/**
 * Errores al sellar
 * @author aiolivaresl
 */
public class SelloException extends Exception {

	public SelloException(String string) {
		super(string);
	}
	
}
