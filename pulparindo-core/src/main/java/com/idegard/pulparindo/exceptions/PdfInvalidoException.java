package com.idegard.pulparindo.exceptions;

/**
 * Archivo pdf invalido o corrupto 
 * @author aiolivaresl
 */
public class PdfInvalidoException extends FirmaException {

	public PdfInvalidoException(String string) {
		super("Pdf invalido: "+string);
	}
	
}
