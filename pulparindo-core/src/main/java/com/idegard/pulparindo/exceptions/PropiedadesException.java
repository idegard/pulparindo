package com.idegard.pulparindo.exceptions;

/**
 * Errores en la definicion de las opciones de firmado
 * @author aiolivaresl
 */
public class PropiedadesException extends FirmaException {

	public PropiedadesException(String string) {
		super("Error de propiedades: "+string);
	}
	
}
