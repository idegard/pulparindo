package com.idegard.pulparindo.exceptions;

/**
 * Errores al cargar llaves publica/privada
 * @author aiolivaresl
 * @see CertificadoInvalidoException
 * @see PrivateKeyException
 */
public class CertificadoException extends Exception {

	public CertificadoException(String string) {
		super(string);
	}
	
}
