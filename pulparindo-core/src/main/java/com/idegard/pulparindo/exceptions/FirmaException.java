package com.idegard.pulparindo.exceptions;

/**
 * Errores al firmado
 * @author aiolivaresl
 * @see PropiedadesException
 * @see PdfInvalidoException
 */
public class FirmaException extends Exception {

	public FirmaException(String string) {
		super(string);
	}
	
}
