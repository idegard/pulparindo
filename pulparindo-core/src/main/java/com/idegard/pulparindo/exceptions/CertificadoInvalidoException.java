package com.idegard.pulparindo.exceptions;

/**
 * Error por la validez del certificado, puede ser por vencido, 
 * que aun no entra en vigor, revocado(temporal o definitivamente), etc.
 * @author aiolivaresl
 */
public class CertificadoInvalidoException extends CertificadoException {

	public CertificadoInvalidoException(String string) {
		super("Certificado no valido: "+string);
	}
	
}
