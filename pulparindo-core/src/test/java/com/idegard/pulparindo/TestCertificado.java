package com.idegard.pulparindo;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;




import org.junit.Test;

import com.idegard.pulparindo.exceptions.CertificadoException;
import com.idegard.pulparindo.exceptions.PrivateKeyException;

public class TestCertificado {

	byte[] certificadoValido= loadCert("cert.p12");
	static String CONTRASENA = "123456";
	
	public TestCertificado() throws URISyntaxException, IOException{}
	
	private byte[] loadCert(String nombre) throws URISyntaxException, IOException{
		File archivo = new File(TestCertificado.class.getResource(nombre).toURI()); 
		byte[] result = new byte[(int)archivo.length()];
		DataInputStream dataIs = new DataInputStream(new FileInputStream(archivo));
		dataIs.readFully(result);
		return result;
	}
	
	@Test(expected=PrivateKeyException.class)
	public void archivoCorrupto() throws CertificadoException{
		new Certificado(new byte[0],CONTRASENA,null);
	}
	
	@Test(expected=PrivateKeyException.class)
	public void contrasenaIncorrecta() throws CertificadoException{
		new Certificado(certificadoValido,CONTRASENA.concat("0"),null);
	}
		
//	@Test(expected=CertificadoInvalidoException.class)
//	public void certificadoInvalido() throws CertificadoException{
//		new Certificado(certificadoValido,"123456",null);
//	}
	
}
