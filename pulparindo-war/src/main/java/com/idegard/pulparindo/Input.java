package com.idegard.pulparindo;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class Input
 */
public class Input extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Input() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<String> parametros = new ArrayList<String>();
		String concat = "";
		//fecha
		concat+=request.getParameter("element_3_1");
		concat+=request.getParameter("element_3_2");
		concat+=request.getParameter("element_3_3");
		parametros.add(concat);
		//nombre
		concat = "";
		concat+=request.getParameter("element_1_1");
		concat+=request.getParameter("element_1_2");
		parametros.add(concat);
		//tel
		concat = "";
		concat+=request.getParameter("element_4_1");
		concat+=request.getParameter("element_4_2");
		concat+=request.getParameter("element_4_3");
		parametros.add(concat);
		//precio
		concat = "";
		concat+=request.getParameter("element_2_1");
		concat+=request.getParameter("element_2_2");
		parametros.add(concat);
		//descripcion
		request.getParameter("element_7");
		parametros.add(concat);
		
		try {
			File archivo = new File(Input.class.getResource("cert.p12").toURI()); 
			byte[] result = new byte[(int)archivo.length()];
			DataInputStream dataIs = new DataInputStream(new FileInputStream(archivo));
			dataIs.readFully(result);
			Map<String, String> res = Sello.sellar(parametros, result, "123456");
			request.setAttribute("original", res.get("original"));
			request.setAttribute("sello", res.get("sellado"));
			} catch (Exception e) {
				request.setAttribute("original", "-Error fatal-");
				request.setAttribute("sello",  "-Error fatal-");
			} 
			request.getRequestDispatcher("/result.jsp").forward(request, response);
	}

}
