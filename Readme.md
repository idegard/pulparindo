Proyecto Pulparindo
=

Proyecto de negocios electronicos correspondiente al segundo parcial. Consiste en una simulacion de sello electronico.


Para ejecutar:

-Clona este repo

-Compila todo el proyecto

	mvn clean install
	
-Ejecuta server

	cd pulparindo-war
	mvn jetty:run
	
-Pruebalo 

http://localhost:8080

___

El proyecto se divide en dos partes core y war:

-El core tiene toda la logica para el sellado de hecho la clase [Sello](pulparindo-core/src/main/java/com/idegard/pulparindo/Sello.java) es la que hace todo pero basicamente todo se resume a estas lineas:

	cert = new Certificado(certificado,contrasena,alias);
	
	dsa = Signature.getInstance("SHA1withRSA");
	dsa.initSign(cert.getPrivateKey());
	
	dsa.update(bytesCadenaOriginal); 
	
	resultadoEnBase64 = Base64.encodeBase64String(dsa.sign());

-El modulo war es solo un servlet que accede al core para hacer el sellado, tiene un certificado por default para realizar el sellado